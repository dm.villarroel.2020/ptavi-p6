import socketserver
import sys
import simplertp
import random
def getargv():
    if len(sys.argv) != 4:
        sys.exit('Usage: python3 server.py <IP> <port> <audio_file>')

    try:
        ip = sys.argv[1]
        port = int(sys.argv[2])
        audio_file = sys.argv[3]

    except AttributeError:
        sys.exit('Usage: python3 server.py <IP> <port> <audio_file>')

    return ip, port, audio_file
class SIPRequest:
    clients = {}
    def __init__(self, data):
        self.data = data
        self.method = None
        self.uri = None
        self.ip = None
        self.header = None
        self.receiver = None
        self.result = None
    def parse(self):
        self._parse_method(self.data)
        self._get_address(self.uri)

    def _get_address(self, uri):
        uri_port = uri.split(':')
        self.header = uri_port[0]
        receiver_ip = uri_port[1].split('@')
        self.receiver = receiver_ip[0]
        self.ip = receiver_ip[1]
        self.clients[self.receiver] = self.ip

    def _parse_method(self, line):
        method_uri = line.split(' ')
        self.method = method_uri[0]
        self.uri = method_uri[1]
class EchoHandler(socketserver.BaseRequestHandler):
    def handle(self):

        ip, port, audio_file = getargv()
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')
        print(f"{received}")
        sip_request = SIPRequest(received)
        sip_request.parse()
        description = f'\n\nv=0 \no=kev@rolland.com 127.0.0.1 \ns=session\nt=0 \nm=audio {self.client_address[1]} RTP'
        typeheader = '\nContent-Type: application/sdp'
        lengthheader = '\nContent-Lenght: 66'
        if sip_request.method == 'INVITE':
            result = 'SIP/2.0 100 TRYING'
            sock.sendto(result.encode('utf-8'), self.client_address)
            result = 'SIP/2.0 180 RING'
            sock.sendto(result.encode('utf-8'), self.client_address)
            result = 'SIP/2.0 200 OK' + typeheader + lengthheader + description
            sock.sendto(result.encode('utf-8'), self.client_address)
        elif sip_request.method == 'BYE':
            result = 'SIP/2.0 200 OK'
            sock.sendto(result.encode('utf-8'), self.client_address)
        elif sip_request.method == 'ACK':
            ALEAT = random.randint(10000, 50000)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=ALEAT)
            audio = simplertp.RtpPayloadMp3(audio_file)
            simplertp.send_rtp_packet(RTP_header, audio,
                                      self.client_address[0],
                                      self.client_address[1])

        else:
            result = 'SIP/2.0 405 Method Not Allowed'
            sock.sendto(result.encode('utf-8'), self.client_address)


def main():

    ip, port, audio_file = getargv()
    try:
        serv = socketserver.UDPServer(('', port), EchoHandler)
        print(f"Listening...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)

if __name__ == "__main__":

    main()
