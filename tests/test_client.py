#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import socketserver
import subprocess
import time
import threading
import unittest

SERVER_IP = '127.0.0.1'
SERVER_PORT = '6003'

# Maximum time we allow the client to run in its own thread (sec.)
MAX_TIME_CLIENT = 10
# Time to wait for server to receive messages (ack, usually)
WAIT_SERVER = 2

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')


class TestCommandLine(unittest.TestCase):
    usage = "Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>\n"

    def test_noargs(self):
        output = subprocess.run(['python3', 'client.py'],
                                timeout=MAX_TIME_CLIENT,
                                cwd=parent_dir, text=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        self.assertEqual(self.usage, output.stdout)

    def test_toomuch_args(self):
        output = subprocess.run(['python3', 'client.py', 'INVITE',
                                 'batman@193.147.73.20:5555', 'extra'],
                                timeout=MAX_TIME_CLIENT,
                                cwd=parent_dir, text=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        self.assertEqual(self.usage, output.stdout)

    def test_bad_method(self):
        output = subprocess.run(['python3', 'client.py', 'INVIT',
                                 'batman@193.147.73.20:5555'],
                                timeout=MAX_TIME_CLIENT,
                                cwd=parent_dir, text=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        self.assertEqual(self.usage, output.stdout)

    def test_bad_address_port(self):
        output = subprocess.run(['python3', 'client.py', 'INVIT',
                                 'batman@193.147.73.20'],
                                timeout=MAX_TIME_CLIENT,
                                cwd=parent_dir, text=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        self.assertEqual(self.usage, output.stdout)

    def test_bad_address_user(self):
        output = subprocess.run(['python3', 'client.py', 'INVIT',
                                 '193.147.73.20:5555'],
                                timeout=MAX_TIME_CLIENT,
                                cwd=parent_dir, text=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        self.assertEqual(self.usage, output.stdout)

    def test_bad_address_sip(self):
        output = subprocess.run(['python3', 'client.py', 'INVIT',
                                 'sip:batman@193.147.73.20:5555'],
                                timeout=MAX_TIME_CLIENT,
                                cwd=parent_dir, text=True,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        self.assertEqual(self.usage, output.stdout)


class HandlerFirst(socketserver.BaseRequestHandler):
    """Receive messages, logging first line of requests"""

    msg = []

    def handle(self):
        self.msg.append(self.request[0])
        sock = self.request[1]
        sock.sendto("SIP/2.0 200 OK\r\n".encode('utf-8'), self.client_address)


class TestWithServerFirst(unittest.TestCase):
    """Test client with a server that checks the first line of requests"""

    def setUp(self):
        self.server = SERVER_IP
        self.port = SERVER_PORT
        HandlerFirst.msg = []
        self.serv = socketserver.UDPServer((self.server, int(self.port)), HandlerFirst)
        threading.Thread(target=self.serv.serve_forever).start()

    def tearDown(self):
        self.serv.server_close()

    def test_invite(self):
        subprocess.run(['python3', 'client.py', 'INVITE',
                        f"user@{self.server}:{self.port}"],
                       timeout=MAX_TIME_CLIENT,
                       cwd=parent_dir)
        time.sleep(WAIT_SERVER)
        self.serv.shutdown()
        self.assertEqual([b'INVITE sip:user@127.0.0.1 SIP/2.0',
                          b'ACK sip:user@127.0.0.1 SIP/2.0'],
                         HandlerFirst.msg)

    def test_bye(self):
        subprocess.run(['python3', 'client.py', 'BYE',
                        f"user@{self.server}:{self.port}"],
                       timeout=MAX_TIME_CLIENT,
                       cwd=parent_dir)
        time.sleep(WAIT_SERVER)
        self.serv.shutdown()
        self.assertEqual([b'BYE sip:user@127.0.0.1 SIP/2.0'],
                         HandlerFirst.msg)


if __name__ == '__main__':
    unittest.main()
